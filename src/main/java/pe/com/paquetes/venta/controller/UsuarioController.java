package pe.com.paquetes.venta.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.sojo.interchange.json.JsonSerializer;
import pe.com.paquetes.venta.bean.UsuarioBean;
import pe.com.paquetes.venta.service.ViajesService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Controller
@RequestMapping("Usuario")
public class UsuarioController {	
	//TODO DECLARACIONES CONTROLLER
	static Log log = LogFactory.getLog(UsuarioController.class.getName());
	
	@Autowired		
	ViajesService viajesService;	
	boolean result;

	//TODO METODO PUT MODIFICAR USUARIO  <- JSON
		@RequestMapping(value="/Rest", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> putUsuario(HttpServletRequest request, HttpServletResponse response) throws Exception  {
		Map<String, Object> mapaResult = new HashMap<String, Object>();		
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.putUsuario");			
			//LECTURA DEL REQUEST BODY
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
			StringBuilder jsonEnviado = new StringBuilder();
			String line;
	        while ((line = reader.readLine()) != null) {
	        	jsonEnviado.append(line).append('\n');
	        }
			//Map<String, Object> dataEnvio = (Map<String, Object>) new JsonSerializer().deserialize(jsonEnviado, Map.class);
			UsuarioBean usuarioNuevo = (UsuarioBean) new JsonSerializer().deserialize(jsonEnviado.toString(), UsuarioBean.class);			
			
			//TODO ACTUALIZAR USUARIO
			result = viajesService.modificarUsuario(usuarioNuevo);
			
			if(result == true) {
				mapaResult.put("status", true);
				mapaResult.put("message", "1 Registro Actualizado");
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "0 Registro Actualizado");
			}
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioController.putUsuario");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.putUsuario");
			
		}			
		return mapaResult;
	}
	
	//TODO METODO POST LOGIN USUARIO EN JSON -> RESPONSEBODY	
	@RequestMapping(value="/Login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> LogUsuario(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> mapaResult = new HashMap<String, Object>();		 
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.getUsuario");
			
			//TODO LECTURA DEL REQUEST BODY JSON <- {"USU":"ATORRES","PWD":"456"}
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
			StringBuilder jsonEnviado = new StringBuilder();
			String line;
	        while ((line = reader.readLine()) != null) {
	        	jsonEnviado.append(line).append('\n');
	        }
	        //----------------------------------------------
	        //TODO SERIALIZAMOS EL JSON -> MAP
			@SuppressWarnings("unchecked")
			Map<String, Object> dataEnvio = (Map<String, Object>) new JsonSerializer().deserialize(jsonEnviado.toString(), Map.class);
			//----------------------------------------------
			//TODO SERVICIO LISTAR USUARIO X CODIGO & PASSWORD		
			UsuarioBean usuarioBean =viajesService.loginUsuario(dataEnvio.get("USU").toString(), dataEnvio.get("PWD").toString());
			//----------------------------------------------
			if(usuarioBean != null) {
				mapaResult.put("status", true);
				mapaResult.put("message", "BIENVENIDO " + usuarioBean.getU_nombre() );
				mapaResult.put("data", usuarioBean);
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "Usuario o Password errados.");
			}
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioController.getUsuario");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.getUsuario");
		}
		
		return mapaResult;
	}
	
	//TODO METODO POST REGISTRAR USUARIO  <- JSON
	@RequestMapping(value="/Rest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> postUsuario(HttpServletRequest request, HttpServletResponse response) throws Exception {
			Map<String, Object> mapaResult = new HashMap<String, Object>();		
			try {
				if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.postUsuario");			
				//LECTURA DEL REQUEST BODY JSON
				BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
				StringBuilder jsonEnviado = new StringBuilder();
				String line;
		        while ((line = reader.readLine()) != null) {
		        	jsonEnviado.append(line).append('\n');
		        }
		        //----------------------------------------------
		        //TODO SERIALIZAMOS EL JSON -> CLASS 
				UsuarioBean usuarioNuevo = (UsuarioBean) new JsonSerializer().deserialize(jsonEnviado.toString(), UsuarioBean.class);			
				//----------------------------------------------
				//TODO SERVICE INSERTAR USUARIO
				result = viajesService.registrarUsuario(usuarioNuevo);
				//----------------------------------------------
				if(result == true) {
					mapaResult.put("status", true);
					mapaResult.put("message", "1 Registro Guardado");
				}else {
					mapaResult.put("status", false);
					mapaResult.put("message", "0 Registro Guardado");
				}			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - UsuarioController.postUsuario");
				log.error(ex, ex);
				mapaResult.put("status", false);
				mapaResult.put("message", ex.getLocalizedMessage());
			}finally {
				if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.postUsuario");			
			}			
			return mapaResult;
		}
	
}
