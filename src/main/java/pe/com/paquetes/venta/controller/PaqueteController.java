package pe.com.paquetes.venta.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.sojo.interchange.json.JsonSerializer;
import pe.com.paquetes.venta.bean.PaqueteBean;
import pe.com.paquetes.venta.service.ViajesService;

@Controller
@RequestMapping("Paciente")
public class PaqueteController {
	//TODO DECLARACIONES CONTROLLER
	static Log log = LogFactory.getLog(UsuarioController.class.getName());
	
	@Autowired		
	ViajesService viajesService;	
	boolean result;
	
	//TODO METODO GET BUSCAR PAQUETE EN JSON -> RESPONSEBODY	
	@RequestMapping(value="/Rest/{p_codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> getUsuario(@PathVariable String p_origen, String p_destino) throws Exception  {	
		Map<String, Object> mapaResult = new HashMap<String, Object>();
		 
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.getUsuario");	
			
			//TODO LISTAR X CODIGO		
			PaqueteBean paquete =viajesService.buscarPaquete(p_origen, p_destino);
			if(paquete != null) {
				mapaResult.put("status", true);
				mapaResult.put("message", "1 Registros Encontrados");
				mapaResult.put("data", paquete);
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "0 Registros Encontrados");
			}
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioController.getUsuario");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.getUsuario");
		}
		
		return mapaResult;
	}

	//TODO METODO POST COMPRAR PAQUETE <- JSON
		@RequestMapping(value="/Rest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
		public @ResponseBody Map<String, Object> postUsuario(HttpServletRequest request, HttpServletResponse response) throws Exception {
			Map<String, Object> mapaResult = new HashMap<String, Object>();		
			try {
				if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.postUsuario");			
				//LECTURA DEL REQUEST BODY JSON
				BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
				StringBuilder jsonEnviado = new StringBuilder();
				String line;
		        while ((line = reader.readLine()) != null) {
		        	jsonEnviado.append(line).append('\n');
		        }
		        //----------------------------------------------
		        //TODO SERIALIZAMOS EL JSON -> CLASS 
				PaqueteBean paqueteNuevo = (PaqueteBean) new JsonSerializer().deserialize(jsonEnviado.toString(), PaqueteBean.class);			
				//----------------------------------------------
				//TODO SERVICE INSERTAR USUARIO
				result = viajesService.comprarPaquete(paqueteNuevo);
				//----------------------------------------------
				if(result == true) {
					mapaResult.put("status", true);
					mapaResult.put("message", "1 Registro Guardado");
				}else {
					mapaResult.put("status", false);
					mapaResult.put("message", "0 Registro Guardado");
				}			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - UsuarioController.postUsuario");
				log.error(ex, ex);
				mapaResult.put("status", false);
				mapaResult.put("message", ex.getLocalizedMessage());
			}finally {
				if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.postUsuario");			
			}			
			return mapaResult;
		}
		
	//TODO METODO PUT MODIFICAR PAQUETE  <- JSON
	@RequestMapping(value="/Rest", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> putUsuario(HttpServletRequest request, HttpServletResponse response) throws Exception  {
				Map<String, Object> mapaResult = new HashMap<String, Object>();		
				try {
					if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.putUsuario");			
					//LECTURA DEL REQUEST BODY
					BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
					StringBuilder jsonEnviado = new StringBuilder();
					String line;
			        while ((line = reader.readLine()) != null) {
			        	jsonEnviado.append(line).append('\n');
			        }
					//Map<String, Object> dataEnvio = (Map<String, Object>) new JsonSerializer().deserialize(jsonEnviado, Map.class);
			        PaqueteBean paqueteNuevo = (PaqueteBean) new JsonSerializer().deserialize(jsonEnviado.toString(), PaqueteBean.class);			
					
					//TODO ACTUALIZAR USUARIO
					result = viajesService.modificarPaquete(paqueteNuevo);
					
					if(result == true) {
						mapaResult.put("status", true);
						mapaResult.put("message", "1 Registro Actualizado");
					}else {
						mapaResult.put("status", false);
						mapaResult.put("message", "0 Registro Actualizado");
					}
					
				} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - UsuarioController.putUsuario");
					log.error(ex, ex);
					mapaResult.put("status", false);
					mapaResult.put("message", ex.getLocalizedMessage());
				}finally {
					if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.putUsuario");
					
				}			
				return mapaResult;
			}

	//TODO METODO DELETE ELIMINAR UN REGISTRO  <- PARAMETER
	@RequestMapping(value="/Rest/{p_codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> delUsuario(@PathVariable String p_codigo) throws Exception {
			Map<String, Object> mapaResult = new HashMap<String, Object>();		
			try {
				if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.delUsuario");			
				//TODO ELIMINAR USUARIO
				result = viajesService.eliminarPaquete(p_codigo);
				
				if(result == true) {
					mapaResult.put("status", true);
					mapaResult.put("message", "1 Registros Eliminado");
				}else {
					mapaResult.put("status", false);
					mapaResult.put("message", "0 Registros Eliminado");
				}			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - UsuarioController.delUsuario");
				log.error(ex, ex);
				mapaResult.put("status", false);
				mapaResult.put("message", ex.getLocalizedMessage());
			}finally {
				if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.delUsuario");
			}		
			return mapaResult;
		}
}
