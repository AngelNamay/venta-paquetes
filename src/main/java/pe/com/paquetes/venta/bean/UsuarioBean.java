package pe.com.paquetes.venta.bean;

import pe.com.paquetes.venta.model.UsuarioModel;

public class UsuarioBean extends UsuarioModel {
	private static final long serialVersionUID = 1L;
	String u_estdes;
	
	public String getU_estdes() {
		return u_estdes;
	}
	public void setU_estdes(String u_estdes) {
		this.u_estdes = u_estdes;
	}
	
}