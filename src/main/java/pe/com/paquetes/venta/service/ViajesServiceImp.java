package pe.com.paquetes.venta.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.paquetes.venta.bean.PaqueteBean;
import pe.com.paquetes.venta.bean.UsuarioBean;
import pe.com.paquetes.venta.dao.ViajesDAO;
import pe.com.paquetes.venta.dao.ViajesDAOImpl;

@Service("UsuarioService")
public class ViajesServiceImp implements ViajesService {
	//TODO DECLARACIONES SERVICE
	private static final Log log = LogFactory.getLog(ViajesServiceImp.class);	
	@Autowired
	private ViajesDAOImpl viajesDAO;
	private boolean result;
	PaqueteBean paqueteBean;
	UsuarioBean usuarioBean;	
	
	//TODO - BUSCAR PAQUETE
	@Override
	public PaqueteBean buscarPaquete(String p_origen, String p_destino) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ViajesServiceImp.buscarPaquete");		
		try {
			paqueteBean = viajesDAO.buscarPaquete(p_origen, p_destino);			
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - ViajesServiceImp.buscarPaquete");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - ViajesServiceImp.buscarPaquete");
		} 	
		return paqueteBean;
	}

	//TODO - COMPRAR PAQUETE
	@Override
	public boolean comprarPaquete(PaqueteBean paquete) throws Exception {
		try {
			result = viajesDAO.comprarPaquete(paquete);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.insertarUsuario");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.insertarUsuario");
		} 	
		return result;
	}

	//TODO - MODIFICAR PAQUETE
	@Override
	public boolean modificarPaquete(PaqueteBean paquete) throws Exception {
		try {
			result = viajesDAO.modificarPaquete(paquete);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - ViajesServiceImp.modificarPaquete");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - ViajesServiceImp.modificarPaquete");
		} 	
		return result;
	}

	//TODO - ELIMINAR PAQUETE
	@Override
	public boolean eliminarPaquete(String p_codigo) throws Exception {
		try {
			result = viajesDAO.eliminarPaquete(p_codigo);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - ViajesServiceImp.eliminarPaquete");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - ViajesServiceImp.eliminarPaquete");
		} 	
		return result;
	}

	//TODO - METODO LOGIN
		@Override
	public UsuarioBean loginUsuario(String u_email, String u_password) throws Exception {
			if (log.isDebugEnabled()) log.debug("Inicio - UsuarioServiceImp.loginUsuario");
			try {
				usuarioBean = viajesDAO.loginUsuario(u_email, u_password);			
			}catch(Exception ex) {
				if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.loginUsuario");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.loginUsuario");
			} 	
			return usuarioBean;
		}

	//TODO - METODO MODIFICAR USUARIO
		@Override
	public boolean modificarUsuario(UsuarioBean usuario) throws Exception {
			try {
				result = viajesDAO.modificarUsuario(usuario);		
			}catch(Exception ex) {
				if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.actualizarUsuario");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.actualizarUsuario");
			} 	
			return result;
		}

	//TODO - METODO REGISTRAR USUARIO
		@Override
	public boolean registrarUsuario(UsuarioBean usuario) throws Exception {
			try {
				result = viajesDAO.registrarUsuario(usuario);		
			}catch(Exception ex) {
				if (log.isDebugEnabled()) log.debug("Error - ViajesServiceImp.registrarUsuario");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - ViajesServiceImp.registrarUsuario");
			} 	
			return result;
		}	
}