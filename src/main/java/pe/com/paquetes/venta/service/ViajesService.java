package pe.com.paquetes.venta.service;

import java.util.List;

import pe.com.paquetes.venta.bean.PaqueteBean;
import pe.com.paquetes.venta.bean.UsuarioBean;

public interface ViajesService {
	
	// METODOS DEL PAQUETE DE VIAJES
	 
    public PaqueteBean buscarPaquete(String p_origen, String p_destino) throws Exception;
    
    public boolean comprarPaquete(PaqueteBean paquete)throws Exception;
    
    public boolean modificarPaquete(PaqueteBean paquete)throws Exception;
    
    public boolean eliminarPaquete(String p_codigo)throws Exception;
    
    // METODOS DEL USUARIO
        
    public UsuarioBean loginUsuario(String u_codigo,String u_password) throws Exception;
    
    public boolean modificarUsuario(UsuarioBean usuario)throws Exception;
    
    public boolean registrarUsuario(UsuarioBean usuario)throws Exception;
    
    // METODOS DEL ADMINISTRADOR
    
    //public List<ViajeBean> listarUsuarios() throws Exception;
}
