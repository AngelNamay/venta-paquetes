package pe.com.paquetes.venta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import pe.com.paquetes.venta.bean.PaqueteBean;
import pe.com.paquetes.venta.bean.UsuarioBean;

public class ViajesDAOImpl implements ViajesDAO{
	//TODO DECLARACIONES DAO
	static Log log = LogFactory.getLog(ViajesDAOImpl.class.getName());	
	private JdbcTemplate jdbcTemplate;
    private int rows;
    private PaqueteBean viajeBean = null;
    private UsuarioBean usuarioBean = null;
    
	//TODO - JDBCTEMPLATE <-> recursos-humanos-descanso-servlet.xml
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
    
    //TODO - BUSCAR PAQUETE POR CIUDAD
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public PaqueteBean buscarPaquete(String p_origen, String p_destino)  throws Exception{
        if (log.isDebugEnabled()) log.debug("Inicio - ViajesDAOImpl.buscarPaquete");
        
        String SQL = "SELECT * FROM PAQUETE WHERE P_ORIGEN = ? AND P_DESTINO = ? ";        
		try {
			viajeBean = (PaqueteBean) jdbcTemplate.queryForObject(SQL, new Object[] { p_origen , p_destino }, new RowMapper(){
			            @Override
			            public PaqueteBean mapRow(ResultSet rs, int rowNum) throws SQLException
			            {
			            	PaqueteBean v = new PaqueteBean();
			            	v.setP_codigo(rs.getString("p_codigo"));
			            	v.setP_origen(rs.getString("p_origen"));
			            	v.setP_destino(rs.getString("p_destino"));
			            	v.setP_hotel(rs.getString("p_hotel"));
			            	v.setP_precio(rs.getInt("p_precio"));
		                	v.setP_estcod(rs.getInt("p_estcod"));
			                return v;
			            }
			        });
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - ViajesDAOImpl.buscarPaquete");
			log.error(ex, ex);
			throw ex;	
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - ViajesDAOImpl.buscarPaquete");
		}
		
		if (log.isDebugEnabled()) log.debug("Inicio - ViajesDAOImpl.buscarPaquete");
		
		return viajeBean;
	}
 
	//TODO - COMPRAR PAQUETE
	@Override
	public boolean comprarPaquete(PaqueteBean paquete) throws Exception {		
        if (log.isDebugEnabled()) log.debug("Inicio - ViajesDAOImpl.comprarPaquete");		
		String SQL = "INSERT INTO PAQUETE VALUES(?,?,?,?,?)";	
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
					 paquete.getP_codigo(),
					 paquete.getP_origen(),
					 paquete.getP_destino(),
					 paquete.getP_hotel(),
					 paquete.getP_precio()});
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - ViajesDAOImpl.comprarPaquete");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - ViajesDAOImpl.comprarPaquete");
		}
		return  rows ==1;
	}
	
	//TODO - MODIFICAR PAQUETE
	@Override
	public boolean modificarPaquete(PaqueteBean paquete) throws Exception {		
        if (log.isDebugEnabled()) log.debug("Inicio - ViajesDAOImpl.modificarPaquete");		
		String SQL = "UPDATE VIAJE SET P_ORIGEN = ?, P_DESTINO = ?, P_HOTEL = ?,  P_PRECIO  WHERE P_CODIGO = ?";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
					paquete.getP_origen(), 
					paquete.getP_destino(), 
					paquete.getP_hotel(), 
					paquete.getP_precio() });
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - ViajesDAOImpl.modificarPaquete");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - ViajesDAOImpl.modificarPaquete");
		}
		return  rows ==1;
	}
	
	//TODO - ELIMINAR PAQUETE
	@Override
	public boolean eliminarPaquete(String p_codigo) throws Exception {		
		if (log.isDebugEnabled()) log.debug("Inicio - ViajesDAOImpl.eliminarPaquete");		
		String SQL = "DELETE FROM VIAJE WHERE V_CODIGO = ? ";		
		try {			
			rows = jdbcTemplate.update(SQL, new Object[] { p_codigo });
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - ViajesDAOImpl.eliminarPaquete");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - ViajesDAOImpl.eliminarPaquete");
		}	     
		return rows == 1;
	}
	
	/*METODOS DE USUARIO*/
	
	//TODO - METODO LOGIN
	@Override
	public UsuarioBean loginUsuario(String u_email, String u_password) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.loginUsuario");		
		
		try {					
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource()).withProcedureName("prcUsuarioLogin");
			 	MapSqlParameterSource params = new MapSqlParameterSource();
			 	params.addValue("in_nombre", u_email);
	            params.addValue("in_passwd", u_password);
	            
			    Map<String, Object> out = jdbcCall.execute(params);

			    usuarioBean= new UsuarioBean();
			    usuarioBean.setU_email(u_email);
			    usuarioBean.setU_password(u_password);
			      
			    usuarioBean.setU_nombre((String) out.get("out_nombre"));
			    usuarioBean.setU_email((String) out.get("out_email"));
			    usuarioBean.setU_estcod((Integer) out.get("out_estcod"));
			
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.selUsuarioByCodigoAndPassword");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.selUsuarioByCodigoAndPassword");
		}	     
		return usuarioBean;
	}
	
	//TODO - METODO MODIFICAR USUARIO
	@Override
	public boolean modificarUsuario(UsuarioBean usuario) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ViajesDAOImpl.modificarUsuario");		
		String SQL = "UPDATE USUARIO SET U_NOMBRE = ?, U_EMAIL = ?, U_PASSWORD = ?,  WHERE U_CODIGO = ?";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] { 
					 usuario.getU_nombre(), 
					 usuario.getU_email(), 
					 usuario.getU_password() });
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - ViajesDAOImpl.modificarUsuario");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - ViajeDAOImpl.modificarUsuario");
		}
		return  rows ==1;
	}
	
	//TODO - METODO REGISTRAR USUARIO
	@Override
	public boolean registrarUsuario(UsuarioBean usuario) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ViajesDAOImpl.registrarUsuario");		
		String SQL = "INSERT INTO USUARIO VALUES(?,?,?,?)";	
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
					 usuario.getU_codigo(),
					 usuario.getU_nombre(),
					 usuario.getU_email(),
					 usuario.getU_password()});
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - ViajesDAOImpl.registrarUsuario");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - ViajesDAOImpl.registrarUsuario");
		}
		return  rows ==1;
	}
	
	/*METODOS DE ADMINISTRADOR*/
	
	/*
	//TODO - LISTAR TODOS
	@Override
	public List<ViajeBean> listarUsuarios() throws Exception{		
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.selectUsuario");
		
		List<ViajeBean> lstUsuarios = null;
		String SQL ="SELECT * FROM tbusuarios";
		
		try {		
			lstUsuarios = jdbcTemplate.query(SQL, new ResultSetExtractor<List<ViajeBean>>(){
	            @Override
	            public List<ViajeBean> extractData(ResultSet rs) throws SQLException, DataAccessException
	            {
	            	List<ViajeBean> list = new ArrayList<ViajeBean>();
	                while (rs.next())
	                {
	                	ViajeBean u = new ViajeBean();
	                	u.setU_id(rs.getString("u_id"));
	                	u.setU_nombre(rs.getString("u_nombre"));
	                	u.setU_password(rs.getString("u_password"));
	                	u.setU_edad(rs.getString("u_edad"));
	                	u.setU_email(rs.getString("u_email"));
	                	u.setU_seguro(rs.getString("u_seguro"));
	                	u.setU_motivo(rs.getString("u_motivo"));
	                	u.setU_tipo(rs.getString("u_tipo"));
	                	u.setU_imagen(rs.getString("u_imagen"));
	                	u.setU_inicio(rs.getTimestamp("u_inicio"));
	                	u.setU_fin(rs.getTimestamp("u_fin"));
	                	u.setU_estcod(rs.getInt("u_estcod"));	                	
	                    list.add(u);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.selectUsuario");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.selectUsuario");
		}
	    return lstUsuarios;
	}*/
	
}
