package pe.com.paquetes.venta.model;

import java.io.Serializable;

//@XmlRootElement
public abstract class PaqueteModel implements Serializable{	
	private static final long serialVersionUID = 1L;
	String 	p_codigo;
	String 	p_origen;
	String 	p_destino;
	String	p_hotel;
	int		p_precio;
	int 	p_estcod;
	
	public String getP_codigo() {
		return p_codigo;
	}
	public void setP_codigo(String p_codigo) {
		this.p_codigo = p_codigo;
	}
	public String getP_origen() {
		return p_origen;
	}
	public void setP_origen(String p_origen) {
		this.p_origen = p_origen;
	}
	public String getP_destino() {
		return p_destino;
	}
	public void setP_destino(String p_destino) {
		this.p_destino = p_destino;
	}
	public String getP_hotel() {
		return p_hotel;
	}
	public void setP_hotel(String p_hotel) {
		this.p_hotel = p_hotel;
	}
	public int getP_precio() {
		return p_precio;
	}
	public void setP_precio(int p_precio) {
		this.p_precio = p_precio;
	}
	public int getP_estcod() {
		return p_estcod;
	}
	public void setP_estcod(int p_estcod) {
		this.p_estcod = p_estcod;
	}
	

}