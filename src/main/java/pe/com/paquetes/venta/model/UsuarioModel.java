package pe.com.paquetes.venta.model;

import java.io.Serializable;

//@XmlRootElement
public abstract class UsuarioModel implements Serializable{	
	private static final long serialVersionUID = 1L;
	String u_codigo;
	String u_nombre;
	String u_email;
	String u_password;
	int u_estcod;
	
	public String getU_codigo() {
		return u_codigo;
	}
	public void setU_codigo(String u_codigo) {
		this.u_codigo = u_codigo;
	}
	public String getU_nombre() {
		return u_nombre;
	}
	public void setU_nombre(String u_nombre) {
		this.u_nombre = u_nombre;
	}
	public String getU_email() {
		return u_email;
	}
	public void setU_email(String u_email) {
		this.u_email = u_email;
	}
	public String getU_password() {
		return u_password;
	}
	public void setU_password(String u_password) {
		this.u_password = u_password;
	}
	public int getU_estcod() {
		return u_estcod;
	}
	public void setU_estcod(int u_estcod) {
		this.u_estcod = u_estcod;
	}
		
}