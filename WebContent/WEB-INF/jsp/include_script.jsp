<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<!-- Mainly scripts -->
<spring:url value="/resources/jquery-v3.1.1/jquery.min.js" var="jqueryJS" />
<!-- <script src="${jqueryJS}" type="text/javascript"></script> -->

<script src="/a/jquery/jquery.min.js" type="text/javascript"></script>

 <script src="/a/twitter-bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

 <script src="/a/metisMenu/metisMenu.min.js" type="text/javascript"></script> 

 <script src="/a/jQuery-slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
 
 <!-- FooTable -->
 <script src="/a/jquery-footable/js/footable.all.min.js" type="text/javascript"></script>
 
 <!-- Flot -->
 <script src="/a/flot/jquery.flot.min.js" type="text/javascript"></script>
 
 <script src="/a/flot.tooltip/jquery.flot.tooltip.min.js" type="text/javascript"></script>
 
 <script src="/a/js/plugins/flot/jquery.flot.spline.js"></script>

 <script src="/a/flot/jquery.flot.resize.min.js" type="text/javascript"></script>

 <script src="/a/flot/jquery.flot.pie.min.js" type="text/javascript"></script>
 
 <!-- Peity -->

 <script src="/a/peity/jquery.peity.min.js" type="text/javascript"></script>
 <script src="/a/js/demo/peity-demo.js"></script>
 
 <!-- Custom and plugin javascript -->
 <script src="/a/js/inspinia.js" type="text/javascript"></script>
 
 <script src="/a/pace/pace.min.js" type="text/javascript"></script>
 <!-- jQuery UI -->
 <script src="/a/jqueryui/jquery-ui.min.js" type="text/javascript"></script>

 <!-- GITTER -->
 <script src="/a/js/plugins/gritter/jquery.gritter.min.js"></script>

 <!-- Sparkline -->
 <script src="/a/jquery-sparklines/jquery.sparkline.min.js" type="text/javascript"></script>

 <!-- Sparkline demo data  -->
 <script src="/a/js/demo/sparkline-demo.js" type="text/javascript"></script>

 <!-- ChartJS-->
 <script src="/a/Chart.js/Chart.min.js" type="text/javascript"></script>
 <!-- Toastr -->
 <script src="/a/toastr.js/js/toastr.min.js" type="text/javascript"></script>
 
 <!-- Datetimepicker JS -->
 <script src="/a/jquery-datetimepicker/jquery.datetimepicker.full.min.js" type="text/javascript"></script>