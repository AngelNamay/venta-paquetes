<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="es">
	<jsp:include page="include_style.jsp"/>
<body>

    <div id="wrapper">
    
        <!-- NavBar -->           
		<jsp:include page="include_navbar.jsp"/>
		
        <div id="page-wrapper" class="gray-bg">
        
            <!-- NavBarHeader -->          
            <jsp:include page="include_navbarheader.jsp"/>
            
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Usuarios</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <a>Mantenimiento</a>
                        </li>
                        <li class="active">
                            <strong>Usuarios</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                        <button id="btnNew" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Nuevo</button>
                        <button id="btnUpdate" class="btn btn-sm btn-default"><i class="fa fa-refresh"></i> Actualizar</button>
                        <button id="btnPrint" class="btn btn-sm btn-default"><i class="fa fa-print"></i> Imprimir</button>
                        <button id="btnExport" class="btn btn-sm btn-default"><i class="fa fa-file-pdf-o"></i> Exportar</button>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Mantenimiento de Usuario</h5>
                        </div>
                        <div class="ibox-content">   
                        
	                         <table class="footable table table-stripped table-responsive toggle-arrow-tiny" data-page-size="8">	
	                         	<thead>                         	
		                         	<tr>
						                <th data-sort-ignore="true">No</th>
						                <th >Codigo</th>
						                <th data-sort-ignore="true">Nombre</th>
						                <th data-sort-ignore="true">Password</th>
						                <th data-sort-ignore="true">Descri</th>
						                <th data-sort-ignore="true">Email</th>
						                <th data-sort-ignore="true">Estado</th>
						                <th data-sort-ignore="true" class="text-right">Acciones</th>  
					                </tr>
				                </thead>
				                <tbody>				                 
					                <c:forEach var="User" items="${listadoUsuarios}" varStatus="status">
					                <tr>
					                    <td>${status.index + 1}</td>
					                    <td>${User.usu_codigo}</td>
					                    <td>${User.usu_nombre}</td>
					                    <td>${User.usu_passwd}</td>
					                    <td>${User.usu_descri}</td>
					                    <td>${User.usu_email}</td>
					                    <td>${User.usu_estdes}</td>
					                    <td class="text-right">
				                            <div class="btn-group">
					                            <button class="btn-white btn btn-xs" id="btnEdit"><i class="fa fa-pencil" aria-hidden="true"></i></button>
					                            <button class="btn-danger btn btn-xs" id="btnDelete"><i class="fa fa-trash" aria-hidden="true"></i></button>
				                            </div>
			                            </td>		                             
					                </tr>
					                </c:forEach>
				                </tbody>
				                <tfoot>
					                <tr>
	                                    <td colspan="7">
	                                        <ul class="pagination pull-right"></ul>
	                                    </td>
	                                </tr>				                
				                </tfoot>             
				            </table>
                        
                        </div>
                    </div>
                </div>
            </div>
            
        </div>        	
            <jsp:include page="include_footer.jsp"/>
        </div>
        </div>                        
	
		<div class="modal inmodal in" id="modDetalle" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	    	<div class="modal-dialog">
	        	<div class="modal-content animated bounceInRight">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
	                    <i class="fa fa-cog modal-icon"></i>
	                    <h4 class="modal-title"><span id="lbl-modal-title"></span></h4>
	                    <small class="font-bold"><span id="lbl-modal-subtitle"></span></small>
	                </div>
	                <div class="modal-body">                    
	                                             
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                    
	                    <button type="button" class="btn btn-primary" id="btn-modal-save" name="btn-modal-save">Guardar</button>
	                </div>
	            </div>
	        </div>
	    </div>
		
	
	<jsp:include page="include_script.jsp"/>
	<script>
		$(document).ready(function() {
		    console.log( "ready!" );
		  	//--- TOASTR
            toastr.options = {
                    closeButton: true,
                    debug: true,
                    progressBar: true,
                    preventDuplicates: false,
                    onclick: null
            };
		    
		  	//--- FOOTABLE            
            $('.footable').footable();

          	//--- EDIT ----------------------------------------------------
            $('body').on('click', '#btnEdit', function (e) {
                e.preventDefault();               
                //--- CARGAR MODAL
                $('#modDetalle').modal('show');
            });
          	//--- DELETE ----------------------------------------------------
            $('body').on('click', '#btnDelete', function (e) {
                e.preventDefault();               
                //--- CARGAR MODAL
                $('#modDetalle').modal('show');
            });
          	//--- NEW ----------------------------------------------------
            $( "#btnNew" ).click(function() {
                           
                //--- CARGAR MODAL
                $('#modDetalle').modal('show');
            });
          	//--- UPDATE ----------------------------------------------------
            $( "#btnUpdate" ).click(function() {
                  
                //--- CARGAR MODAL
                $('#modDetalle').modal('show');
            });
		});
	</script>
</body>
</html>